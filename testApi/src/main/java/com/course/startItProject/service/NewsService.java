package com.course.startItProject.service;

import com.course.startItProject.entity.News;
import com.course.startItProject.entity.Project;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface NewsService {

    List<News> findByProject(Project project);

    List<News> get5latestNews();

    void save(News news);

    void save(News news, String imageUrl, Project project);
}
