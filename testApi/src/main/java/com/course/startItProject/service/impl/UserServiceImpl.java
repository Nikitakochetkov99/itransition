package com.course.startItProject.service.impl;

import com.course.startItProject.entity.User;
import com.course.startItProject.repo.UserRepository;
import com.course.startItProject.entity.enums.Role;
import com.course.startItProject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.util.Collections;
import java.util.List;

@Service
public class UserServiceImpl implements UserDetailsService, UserService {

    @Autowired
    private SessionServiceImpl sessionService;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SessionRegistry sessionRegistry;

    @Autowired
    private RolesServiceImpl rolesService;

    @Autowired
    private ProjectServiceImpl projectService;

    @Autowired
    private CloudinaryServiceImpl cloudinaryService;

    @Override
    public void saveUser(User user) {
        user.setRoles(Collections.singleton(Role.USER));
        user.setUrl("https://res.cloudinary.com/dy8wypu9q/image/upload/v1620865971/Profile01-512_v9y6ua.png");
        user.setActive(true);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    @Override
    public String getProfileUrlOfCurrentUser() {
        User user = sessionService.getCurrentUser();
        if (user != null) {
            return user.getUrl();
        }
        return "";
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(long userId) {
        return userRepository.findById(userId);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username);
    }

    @Override
    public User findByUsername(String name) {
        return userRepository.findByUsername(name);
    }

    @Override
    public void update(User user) {
        userRepository.save(user);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public void deleteUserWithProjects(Long userId) {
        projectService.deleteListOfProjects(projectService.findByAuthor(userService.findById(userId)));
        sessionService.expireUsersSessionById(userId, sessionRegistry);
        userService.delete(userService.findById(userId));
    }

    @Override
    public void blockUser(Long userId) {
        sessionService.expireUsersSessionById(userId, sessionRegistry);
        User userToBlock = userService.findById(userId);
        userToBlock.setActive(false);
        userService.update(userToBlock);
    }

    public void unblockUser(Long userId) {
        User userToUnblock = userService.findById(userId);
        userToUnblock.setActive(true);
        userService.update(userToUnblock);
    }

    public void changeRole(Long userId) {
        User user = userService.findById(userId);
        Object[] roleArray = user.getRoles().toArray();
        if (Role.ADMIN.equals(roleArray[0])) {
            rolesService.changeRoleToUser(user);
        } else {
            rolesService.changeRoleToAdmin(user);
        }
        userService.update(user);
    }

    public void updatePic(Long userId, MultipartFile file) {
        User user = userService.findById(userId);
        user.setUrl(cloudinaryService.uploadFile(file));
        userService.update(user);
    }

}
