package com.course.startItProject.service;

import com.course.startItProject.entity.History;
import com.course.startItProject.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface HistoryService {

    List<History> findByUser(User user);

    void save(Long bonusId, History history);
}
