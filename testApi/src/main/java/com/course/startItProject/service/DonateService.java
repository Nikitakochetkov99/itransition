package com.course.startItProject.service;

import com.course.startItProject.entity.Donate;
import com.course.startItProject.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DonateService {

    void save(Donate donate, Long projectId, User user);

    List<Donate> get5latestDonates();
}
