package com.course.startItProject.service;

import com.course.startItProject.entity.Project;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ImageService {

    void saveImage(Project project, String url);

    List<String> getUrlsOfProject(Project project);
}
