package com.course.startItProject.service.impl;

import com.course.startItProject.service.YouTubeService;
import org.springframework.stereotype.Service;

@Service
public class YouTubeServiceImpl implements YouTubeService {

    @Override
    public String adaptLink(String url) {
        return url.replace("watch?v=", "embed/");
    }
}
