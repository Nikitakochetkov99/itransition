package com.course.startItProject.service;

import com.course.startItProject.entity.User;
import org.springframework.stereotype.Service;

@Service
public interface RolesService {

    void changeRoleToAdmin(User user);

    void changeRoleToUser(User user);
}
