package com.course.startItProject.service.impl;

import com.course.startItProject.entity.User;
import com.course.startItProject.entity.enums.Role;
import com.course.startItProject.service.RolesService;
import org.springframework.stereotype.Service;

@Service
public class RolesServiceImpl implements RolesService {

    @Override
    public void changeRoleToAdmin(User user) {
        user.getRoles().clear();
        user.getRoles().add(Role.ADMIN);
    }

    @Override
    public void changeRoleToUser(User user) {
        user.getRoles().clear();
        user.getRoles().add(Role.USER);
    }
}
