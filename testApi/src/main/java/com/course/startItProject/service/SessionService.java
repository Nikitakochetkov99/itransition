package com.course.startItProject.service;

import com.course.startItProject.entity.User;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Service;

@Service
public interface SessionService {

    User getCurrentUser();

    void expireUsersSessionById(long userId, SessionRegistry sessionRegistry);
}
