package com.course.startItProject.service.impl;

import com.course.startItProject.entity.User;
import com.course.startItProject.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SessionServiceImpl implements SessionService {

    @Autowired
    private UserServiceImpl userService;

    @Override
    public User getCurrentUser() {
        return userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    @Override
    public void expireUsersSessionById(long userId, SessionRegistry sessionRegistry) {
        List<Object> principals = sessionRegistry.getAllPrincipals();
        String username = userService.findById(userId).getUsername();
        User userToDelete = new User();
        for (Object principal : principals) {
            User temp = (User) principal;
            if (temp.getUsername().equals(username)) {
                userToDelete = temp;
            }
        }
        if (userToDelete.getId() != null) {
            sessionRegistry.getAllSessions(userToDelete, false).get(0).expireNow();
        }
    }
}
