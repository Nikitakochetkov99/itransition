package com.course.startItProject.service.impl;

import com.course.startItProject.entity.Project;
import com.course.startItProject.entity.Rating;
import com.course.startItProject.entity.User;
import com.course.startItProject.repo.RatingRepository;
import com.course.startItProject.service.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RatingServiceImpl implements RatingService {

    @Autowired
    private RatingRepository ratingRepository;

    @Autowired
    private SessionServiceImpl sessionService;

    @Override
    public void save(Rating rating, Project project){
        rating.setProject(project);
        rating.setUser(sessionService.getCurrentUser());
        ratingRepository.save(rating);
    }

    @Override
    public Rating findByProjectAndUser(Project project, User user){
        try {
           return ratingRepository.findByProjectAndUser(project ,user);
        }
        catch (NullPointerException e){
            return null;
        }
    }

    @Override
    public Double getAverageRating(Project project){
        return  ratingRepository.findByProject(project)
                .stream()
                .map(Rating::getValue)
                .mapToDouble(value -> value)
                .average()
                .orElse(0.0);
    }
}
