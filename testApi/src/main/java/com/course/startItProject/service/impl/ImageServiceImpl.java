package com.course.startItProject.service.impl;

import com.course.startItProject.entity.Image;
import com.course.startItProject.entity.Project;
import com.course.startItProject.repo.ImageRepository;
import com.course.startItProject.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ImageServiceImpl implements ImageService {

    @Autowired
    private ImageRepository imageRepository;

    @Override
    public void saveImage(Project project, String url) {
        imageRepository.save(new Image(project, url));
    }

    @Override
    public List<String> getUrlsOfProject(Project project) {
        return imageRepository.findByProject(project).stream()
                .map(Image::getUrl)
                .collect(Collectors.toList());
    }
}
