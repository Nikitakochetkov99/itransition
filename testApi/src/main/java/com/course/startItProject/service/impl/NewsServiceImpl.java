package com.course.startItProject.service.impl;

import com.course.startItProject.entity.News;
import com.course.startItProject.entity.Project;
import com.course.startItProject.repo.NewsRepository;
import com.course.startItProject.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsRepository newsRepository;

    @Override
    public List<News> findByProject(Project project){
        return newsRepository.findByProject(project);
    }

    @Override
    public List<News> get5latestNews(){
        return newsRepository.findFirst5ByOrderByIdDesc();
    }

    @Override
    public void save(News news){
        newsRepository.save(news);
    }

    @Override
    public void save(News news, String imageUrl, Project project){
        news.setImageUrl(imageUrl);
        news.setProject(project);
        newsRepository.save(news);
    }
}
