package com.course.startItProject.service;

import com.course.startItProject.entity.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public interface UserService {

    void saveUser(User user);

    String getProfileUrlOfCurrentUser();

    List<User> findAll();

    User findById(long userId);

    UserDetails loadUserByUsername(String username);

    User findByUsername(String name);

    void update(User user);

    void delete(User user);

    void deleteUserWithProjects(Long userId);

    void blockUser(Long userId);

    void unblockUser(Long userId);

    void changeRole(Long userId);

    void updatePic(Long userId, MultipartFile file);
}
