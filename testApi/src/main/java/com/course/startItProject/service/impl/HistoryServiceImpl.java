package com.course.startItProject.service.impl;

import com.course.startItProject.entity.Bonus;
import com.course.startItProject.entity.History;
import com.course.startItProject.entity.Project;
import com.course.startItProject.entity.User;
import com.course.startItProject.repo.HistoryRepository;
import com.course.startItProject.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HistoryServiceImpl implements HistoryService {

    @Autowired
    private HistoryRepository historyRepository;

    @Autowired
    private BonusServiceImpl bonusService;

    @Autowired
    private ProjectServiceImpl projectService;

    @Autowired
    private SessionServiceImpl sessionService;

    @Override
    public List<History> findByUser(User user) {
        return historyRepository.findByUser(user);
    }

    @Override
    public void save(Long bonusId, History history) {
        User user = sessionService.getCurrentUser();
        Bonus bonus = bonusService.findById(bonusId);
        Project project = bonus.getProject();
        project.setReached(project.getReached() + bonus.getCost());
        projectService.update(project);
        history.setBonus(bonus);
        history.setUser(user);
        historyRepository.save(history);
    }
}
