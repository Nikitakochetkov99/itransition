package com.course.startItProject.service.impl;

import com.course.startItProject.entity.Donate;
import com.course.startItProject.entity.Project;
import com.course.startItProject.entity.User;
import com.course.startItProject.repo.DonateRepository;
import com.course.startItProject.service.DonateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DonateServiceImpl implements DonateService {

    @Autowired
    private DonateRepository donateRepository;

    @Autowired
    private ProjectServiceImpl projectService;

    @Override
    public void save(Donate donate, Long projectId, User user){
        Project project = projectService.findProjectById(projectId);
        donate.setProject(project);
        donate.setUser(user);
        donateRepository.save(donate);

        project.setReached(project.getReached() + (double) donate.getSum());
        projectService.update(project);

    }

    @Override
    public List<Donate> get5latestDonates(){
        return donateRepository.findFirst5ByOrderByIdDesc();
    }
}
