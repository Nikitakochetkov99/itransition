package com.course.startItProject.service.impl;

import com.course.startItProject.entity.enums.Categories;
import com.course.startItProject.entity.Project;
import com.course.startItProject.entity.Rating;
import com.course.startItProject.entity.User;
import com.course.startItProject.repo.ProjectRepository;
import com.course.startItProject.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;

@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ImageServiceImpl imageService;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private CloudinaryServiceImpl cloudinaryService;

    @Autowired
    private YouTubeServiceImpl youTubeService;

    @Autowired
    private RatingServiceImpl ratingService;

    public List<Project> getAll() {
        return projectRepository.findAll()
                .stream()
                .peek(project -> project.setImageUrls(imageService.getUrlsOfProject(project)))
                .collect(toList());
    }

    @Override
    public List<Project> findByAuthor(User user) {
        return projectRepository.findByAuthor(user);
    }

    @Override
    public Project findProjectById(long projectId) {
        return projectRepository.findProjectById(projectId);
    }

    @Override
    public List<Project> findByCategories(Categories category) {
        return projectRepository.findByCategories(category);
    }

    @Override
    public List<Project> findTop3ByRating(){
        return projectRepository.findTop3ByOrderByRatingDesc()
                .stream()
                .peek(project -> project.setImageUrls(imageService.getUrlsOfProject(project)))
                .collect(toList());
    }

    @Override
    public void deleteProject(Long id) {
        projectRepository.deleteById(id);
    }

    @Override
    public void update(Project project) {
        projectRepository.save(project);
    }

    @Override
    public void save(User user, Project project, MultipartFile[] file) {
        stream(file).forEach(multipartFile -> imageService.saveImage(project, cloudinaryService.uploadFile(multipartFile)));
        project.setAuthor(user);
        String videoLink = project.getVideoLink();
        project.setVideoLink(youTubeService.adaptLink(videoLink));
        projectRepository.save(project);
    }

    @Override
    public List<Project> getMyProjects() {
        return projectRepository.findByAuthor(userService.findByUsername(getContext().getAuthentication().getName()));
    }

    @Override
    public void editProject(Project project, Long projectId) {
        Project projectToEdit = projectRepository.findProjectById(projectId);
        projectToEdit.setProjectName(project.getProjectName());
        projectToEdit.setCategories(project.getCategories());
        projectToEdit.setShortDisc(project.getShortDisc());
        projectToEdit.setGoal(project.getGoal());
        projectToEdit.setVideoLink(project.getVideoLink());
        projectToEdit.setDurationDate(project.getDurationDate());
        projectToEdit.setFullDisc(project.getFullDisc());
        projectRepository.save(projectToEdit);
    }

    @Override
    public void deleteListOfProjects(List<Project> projects) {
        projects.forEach(project -> projectRepository.delete(project));
    }

    @Override
    public void editRating(Long projectId, Rating rating) {
        Project project = projectRepository.findProjectById(projectId);
        ratingService.save(rating, project);
        project.setRating(ratingService.getAverageRating(project).intValue());
        projectRepository.save(project);
    }
}
