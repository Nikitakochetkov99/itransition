package com.course.startItProject.service;

import com.course.startItProject.entity.Project;
import com.course.startItProject.entity.Rating;
import com.course.startItProject.entity.User;
import org.springframework.stereotype.Service;

@Service
public interface RatingService {

    void save(Rating rating, Project project);

    Rating findByProjectAndUser(Project project, User user);

    Double getAverageRating(Project project);
}
