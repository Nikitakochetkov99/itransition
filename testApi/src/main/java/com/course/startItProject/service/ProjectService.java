package com.course.startItProject.service;

import com.course.startItProject.entity.enums.Categories;
import com.course.startItProject.entity.Project;
import com.course.startItProject.entity.Rating;
import com.course.startItProject.entity.User;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public interface ProjectService {

    List<Project> getAll();

    List<Project> findByAuthor(User user);

    Project findProjectById(long projectId);

    List<Project> findByCategories(Categories category);

    List<Project> findTop3ByRating();

    void deleteProject(Long id);

    void update(Project project);

    void save(User user, Project project, MultipartFile[] file);

    List<Project> getMyProjects();

    void editProject(Project project, Long projectId);

    void deleteListOfProjects(List<Project> projects);

    void editRating(Long projectId, Rating rating);
}
