package com.course.startItProject.service;

import org.springframework.stereotype.Service;

@Service
public interface YouTubeService {

    String adaptLink(String url);
}
