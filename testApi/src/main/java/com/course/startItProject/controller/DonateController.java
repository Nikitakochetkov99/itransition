package com.course.startItProject.controller;

import com.course.startItProject.entity.*;
import com.course.startItProject.service.impl.BonusServiceImpl;
import com.course.startItProject.service.impl.DonateServiceImpl;
import com.course.startItProject.service.impl.HistoryServiceImpl;
import com.course.startItProject.service.impl.ProjectServiceImpl;
import com.course.startItProject.service.impl.SessionServiceImpl;
import com.course.startItProject.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DonateController {

    @Autowired
    private DonateServiceImpl donateServiceImpl;

    @Autowired
    private HistoryServiceImpl historyServiceImpl;

    @PostMapping("/donate")
    public String donate(@RequestParam("id") @ModelAttribute long projectId, @ModelAttribute Donate donate,
                         @AuthenticationPrincipal User user, ModelMap modelMap) {

        donateServiceImpl.save(donate, projectId, user);
        modelMap.addAttribute("Success", "You've donated successfully!");
        return "redirect:/project?id=" + projectId;
    }

    @PostMapping("/buy")
    public String buyBonus(@RequestParam("bonusId") long bonusId, @ModelAttribute("history") History history) {
        historyServiceImpl.save(bonusId, history);
        return "redirect:/project?id=" + history.getBonus().getProject().getId();
    }
}
