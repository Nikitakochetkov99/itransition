package com.course.startItProject.controller;

import com.course.startItProject.entity.*;
import com.course.startItProject.service.impl.BonusServiceImpl;
import com.course.startItProject.service.impl.CloudinaryServiceImpl;
import com.course.startItProject.service.impl.DonateServiceImpl;
import com.course.startItProject.service.impl.ImageServiceImpl;
import com.course.startItProject.service.impl.NewsServiceImpl;
import com.course.startItProject.service.impl.ProjectServiceImpl;
import com.course.startItProject.service.impl.RatingServiceImpl;
import com.course.startItProject.service.impl.SessionServiceImpl;
import com.course.startItProject.service.impl.UserServiceImpl;
import com.course.startItProject.service.impl.YouTubeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import static java.util.Arrays.stream;

@Controller
public class ProjectController {

    @Autowired
    private UserServiceImpl userServiceImpl;

    @Autowired
    private ProjectServiceImpl projectServiceImpl;

    @Autowired
    private CloudinaryServiceImpl cloudinaryServiceImpl;

    @Autowired
    private ImageServiceImpl imageServiceImpl;

    @Autowired
    private BonusServiceImpl bonusServiceImpl;

    @Autowired
    private RatingServiceImpl ratingServiceImpl;

    @Autowired
    private SessionServiceImpl sessionServiceImpl;

    @Autowired
    private NewsServiceImpl newsServiceImpl;

    @Autowired
    private DonateServiceImpl donateServiceImpl;

    @GetMapping("")
    public String getFirstPage(ModelMap modelMap) {
        modelMap.addAttribute("listOfProject", projectServiceImpl.findTop3ByRating());
        modelMap.addAttribute("donates", donateServiceImpl.get5latestDonates());
        modelMap.addAttribute("profileImg", userServiceImpl.getProfileUrlOfCurrentUser());
        return "startPage";
    }

    @PostMapping("/addCampaign")
    public String saveProject(@AuthenticationPrincipal User user, @ModelAttribute Project project, @RequestParam("file") MultipartFile[] file) {
        projectServiceImpl.save(user, project, file);
        return ("redirect:/");
    }

    @GetMapping("/addCampaign")
    public String addCampaign() {
        return "createCampaign";
    }

    @PostMapping("/deleteProject")
    public String deleteProject(@RequestParam("id") Long projectId) {
        projectServiceImpl.deleteProject(projectId);
        return "redirect:/";
    }

    @GetMapping("/project")
    public String getProjectPage(@RequestParam("id") long projectId, ModelMap model) {
        Project project = projectServiceImpl.findProjectById(projectId);
        project.setImageUrls(imageServiceImpl.getUrlsOfProject(project));
        model.addAttribute("rating", ratingServiceImpl.findByProjectAndUser(project, sessionServiceImpl.getCurrentUser()));
        model.addAttribute("bonuses", bonusServiceImpl.findByProject(project));
        model.addAttribute("project", project);
        model.addAttribute("profileImg", userServiceImpl.getProfileUrlOfCurrentUser());
        return "project";
    }

    @PostMapping("/editform")
    public String getEditForm(@RequestParam("id") long projectId, ModelMap modelMap) {
        modelMap.addAttribute("project", projectServiceImpl.findProjectById(projectId));
        return "editform";
    }

    @PostMapping("/edit")
    public String editProject(@RequestParam("id") long projectId, @ModelAttribute("project") Project project) {
        projectServiceImpl.editProject(project, projectId);
        return "redirect:/project?id=" + projectId;
    }

    @PostMapping("addbonus")
    public String addBonus(@RequestParam("id") long projectId, @ModelAttribute("bonus") Bonus bonus) {
        bonusServiceImpl.save(bonus, projectId);
        return "redirect:/project?id=" + projectId;
    }

    @PostMapping("rate")
    public String rate(@RequestParam("projectId") long projectId, @ModelAttribute("rating") Rating rating) {
        projectServiceImpl.editRating(projectId, rating);
        return "redirect:/project?id=" + projectId;
    }
}

