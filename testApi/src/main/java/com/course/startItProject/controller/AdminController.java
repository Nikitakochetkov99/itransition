package com.course.startItProject.controller;

import com.course.startItProject.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@PreAuthorize("hasRole('ADMIN')")
public class AdminController {

    @Autowired
    private UserServiceImpl userServiceImpl;

    @RequestMapping("/admin")
    public String adminPage(ModelMap modelMap) {
        modelMap.addAttribute("users", userServiceImpl.findAll());
        modelMap.addAttribute("profileImg", userServiceImpl.getProfileUrlOfCurrentUser());
        return "admin";
    }

    @PostMapping("/deleteUser")
    public String deleteUser(@RequestParam("id") long userId) {
        userServiceImpl.deleteUserWithProjects(userId);
        return "redirect:/admin";
    }

    @PostMapping("/blockUser")
    public String blockUser(@RequestParam("id") long userId) {
        userServiceImpl.blockUser(userId);
        return "redirect:/admin";
    }

    @PostMapping("/unblockUser")
    public String unblockUser(@RequestParam("id") long userId) {
        userServiceImpl.unblockUser(userId);
        return "redirect:/admin";
    }

    @PostMapping("/changeRole")
    public String changeRole(@RequestParam("id") long userId) {
        userServiceImpl.changeRole(userId);
        return "redirect:/admin";
    }
}
