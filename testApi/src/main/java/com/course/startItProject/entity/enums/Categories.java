package com.course.startItProject.entity.enums;

public enum Categories {
    Tech,
    Food,
    Sport,
    Science,
    Society,
    Design,
    Games,
    Music,
    Cinema,
    Photo,
    Animals,
    Literature,
    Others
}
